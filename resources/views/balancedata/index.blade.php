@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            @include('balancedata.input')
            @include('balancedata.list')
        </div>
    </div>
</div>
@endsection